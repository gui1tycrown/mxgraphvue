// 接口拼接
const url = process.env.VUE_APP_BASE_API; // 根据开发/生产获取对应的URL
export default {};
export const postfix = {
  /** 获取数据列表 **/
  getActivityList: `${url}Activity/getActivityList`, // 获取活动列表
  getFilePath: `${url}Activity/getFilePath`, // 获取流程图文件路径
  getFileXML: `${url}Activity/getFileXML`, // 获取流程图文件内容
  getPTRTree: `${url}PTR/getPTRTree`, // 获取PTR树结构
   /** 更新数据 **/
   updateActivity: `${url}Activity/updateActivity`, // 更新活动
  /** 保存数据 **/
  saveXML: `${url}Activity/saveXML`, // 保存流程图文件内容
};