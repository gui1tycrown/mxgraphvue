import request from './request';
import { postfix } from './index';

/** 获取数据列表 **/

// 获取活动列表
/*** 
 * prjCode 项目阶段状态
 * xmlBlobxml文档内容（当接口不为saveXML时可为空）
 * returns: {
    "detailMsg": "",
    "maprow": {},
    "msg": "",
    "objOther": {},
    "pageNumber": 0,
    "pageSize": 0,
    "rows": [],
    "total": 0
  }
 * **/
// 获取活动列表
export async function getApiActivityList(data) {
  return request({
    url: postfix.getActivityList,
    method: 'post',
    params: data
  });
}

// 获取流程图文件路径
export async function getApiFilePath(data) {
  return request({
    url: postfix.getFilePath,
    method: 'post',
    params: data
  });
}

// 获取流程图文件内容
export async function getApiFileXML(data) {
  return request({
    url: postfix.getFileXML,
    method: 'post',
    params: data
  });
}

// 获取流程图文件内容
export async function getApiPTRTree(data) {
  return request({
    url: postfix.getPTRTree,
    method: 'post',
    params: data
  });
}

 /** 更新数据 **/

// 更新活动
export async function updateApiActivity(data) {
  return request({
    url: postfix.updateActivity,
    method: 'post',
    params: data
  });
}

 /** 保存数据 **/

// 保存流程图文件内容
export async function saveApiXML(data) {
  return request({
    url: postfix.saveXML,
    method: 'post',
    params: data
  });
}
